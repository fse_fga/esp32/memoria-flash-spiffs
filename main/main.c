#include <stdio.h>
#include "esp_spiffs.h"
#include "esp_log.h"

#define TAG "SPIFFS"

void app_main(void)
{
    // Configurações do sistema de arquivos
    esp_vfs_spiffs_conf_t spiffs_config = {
        .base_path = "/spiffs",
        .partition_label = NULL,
        .max_files = 10,
        .format_if_mount_failed = true,
    };
    esp_vfs_spiffs_register(&spiffs_config);

    FILE *arquivo1 = fopen("/spiffs/arquivo1.txt", "r");
    if(arquivo1 == NULL)
    {
        ESP_LOGE(TAG, "Não foi possível abrir o arquivo");
    }
    else 
    {
        char linha[256];
        while(fgets(linha, sizeof(linha), arquivo1) != NULL)
        {
            printf(linha);
        }
        fclose(arquivo1);
    }

    // Acessando o arquivo 2

    FILE *arquivo2 = fopen("/spiffs/pasta/arquivo2.txt", "r");
    if(arquivo2 == NULL)
    {
        ESP_LOGE(TAG, "Não foi possível abrir o arquivo");
    }
    else 
    {
        char linha2[256];
        while(fgets(linha2, sizeof(linha2), arquivo2) != NULL)
        {
            printf(linha2);
        }
        fclose(arquivo2);
    }

    esp_vfs_spiffs_unregister(NULL);
    
}
